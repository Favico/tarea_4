"""-----------------------Autor Christian Condoy-------------------------------
---------------------Email christian.condoy@unl.edu.ec--------------------------
Ejercicio 2: Escribe otro programa que pida una lista de números como
la anterior y al final muestre por pantalla el máximo y mínimo de los
números, en vez de la media."""
i=0
acu=0
lista=[]

while True:
    numero = input("Ingrese un Número: ")
    lista.append(numero)
    if numero.lower() in "fin":
        break
    try:
        acu = acu+int(numero)
        i = i+1
        mayor = max(lista)
        menor = min(lista)
    except ValueError:
        print("Entrada Invalida")
print("Proceso Terminado")

print("la suma de los número es: ", acu)
print("la cantidad de los números introducidos es: ", i)
print("Numero mayor", mayor)
print("Numero menor",menor)