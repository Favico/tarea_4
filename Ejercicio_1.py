"""-----------------------Autor Christian Condoy-------------------------------
---------------------Email christian.condoy@unl.edu.ec--------------------------
Ejercicio 1: Escribe un programa que lea repetidamente números hasta
que el usuario introduzca “fin”. Una vez se haya introducido “fin”,
muestra por pantalla el total, la cantidad de números y la media de
esos números. Si el usuario introduce cualquier otra cosa que no sea un
número, detecta su fallo usando try y except, muestra un mensaje de
error y pasa al número siguiente.
Introduzca un número: 4
Introduzca un número: 5
Introduzca un número: dato erróneo
Entrada inválida
Introduzca un número: 7
Introduzca un número: fin
16 3 5.33333333333"""

i=0
acu=0
while True:
    numero = input("ingrese un número (o fin para terminar): ")
    if numero.lower() in "fin":
        break
    try:
        acu = acu+int(numero)
        i = i+1
    except ValueError:
        print("Entrada Invalida")
print("proceso terminado")

print("la suma de los número es: ", acu)
print("la cantidad de los números introducidos es: ", i)
print("el promedio de los números ingresados es: ", acu / i)